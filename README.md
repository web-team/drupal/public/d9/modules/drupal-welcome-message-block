# Drupal Welcome Message Block 

Drupal module that implements and displays a welcome block for all new Drupal
websites within CERN. The block is automatically deployed in the front-page of 
CERN Theme after installation.